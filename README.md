# Snacky

**本项目是基于开源项目Snacky进行ohos的移植和开发的，可以通过项目标签以及github地址（https://github.com/matecode/Snacky
）追踪到原项目版本**

## 项目介绍

- 项目名称：Snacky
- 所属系列：ohos的第三方组件适配移植
- 功能：用于在布局中添加Snackbar,提供一些模板设计，如错误、警告、信息和成功以及一些自定义选项
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/matecode/Snacky
- 原项目基线版本：v1.1.6, sha1: 50ccaa9818720a66514ddfbbf6f2857c75fc3928

- 编程语言：Java
- 外部库依赖：无

#### 效果展示

![gif](screenshot/operation.gif)

## 安装教程

方法1.

1. 编译har包:library.har。
2. 启动 DevEco Studio，将编译的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
}
```
4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方法2.

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址
```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/'
    }
}
```
2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
dependencies {
    implementation 'de.mateware.ohos:snacky:1.0.2'
}
```

## 使用说明

### 1. 初始化
        首先Snacky.builder()获得一个Builder。snackbar设置视图snackbar.setComponent(builder.view)

### 2. set一些属性，SnackBar对象

       .setBackgroundColor() 颜色

       .setText() 文字的字集或整数

       .setTextColor() 颜色或ColorStateList

       .setTextSize() SP或ComplexUnit中的大小

       .setTextTypeface() 字体

       .setTextTypefaceStyle()字体中的NORMAL，BOLD，ITALIC，BOLD_ITALIC

       .setMaxLines() Snackbar的最大行数，默认情况下处于关闭状态

       .centerText() 使文字居中

       .setActionText()

       .setActionTextColor() 颜色或ColorStateList

       .setActionTextSize()

       .setActionTextTypeface()

       .setActionTextTypefaceStyle() 就像.setTextTypefaceStyle（）

       .setActionClickListener(View.OnClickListener)

       .setDuration(Snacky.DURATION) 短，长，不确定

       .setIcon() 要显示绘画，我认为最好使用尺寸为24dp的小绘画

       建造
       .build() 为您提供了Snackbar，但您也可以使用一些预定义的模板：

       .success()

       .error()

       .info()

       .warning()

       所有这些都为您提供了一个SnackBar对象，但是如果您之前没有自定义它们，则可以设置一些预定义的值。

       .addCallback(SnackBar.Callback) 向小吃栏添加回调

       .show() 显示SnackBar
        ……

## 版本迭代
- v1.0.2

## 版权和许可信息

Apache license version 2.0
