/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.mateware.snacky;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.window.dialog.PopupDialog;
import ohos.app.Context;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SnackyTest {

    private Context context = null;

    @Before
    public void setUp() throws Exception {
        context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
    }

    @After
    public void tearDown() throws Exception {
        context = null;
    }

    @Test
    public void make() {
        IAbilityDelegator abilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        abilityDelegator.runOnUIThreadSync(new Runnable() {
            @Override
            public void run() {
                PopupDialog dialog = Snacky.builder()
                        .setContext(context)
                        .setText("You have to use CoordinatorLayout as usual to have a FAB floating with the Snackbar.")
                        .setDuration(Snacky.LENGTH_SHORT)
                        .build();
                dialog.show();
                assertTrue(dialog.isShowing());
            }
        });
    }

    @Test
    public void builder() {
        assertNotNull(Snacky.builder());
    }

    @Test
    public void success() {
        IAbilityDelegator abilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        abilityDelegator.runOnUIThreadSync(new Runnable() {
            @Override
            public void run() {
                Snacky.Builder builder = Snacky.builder();
                PopupDialog dialog = builder.setContext(context)
                        .success();
                dialog.show();
                Snacky.Type type = builder.getType();
                assertEquals(Snacky.Type.SUCCESS, type);
            }
        });
    }

    @Test
    public void info() {
        IAbilityDelegator abilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        abilityDelegator.runOnUIThreadSync(new Runnable() {
            @Override
            public void run() {
                Snacky.Builder builder = Snacky.builder();
                PopupDialog dialog = builder.setContext(context)
                        .info();
                dialog.show();
                Snacky.Type type = builder.getType();
                assertEquals(Snacky.Type.INFO, type);
            }
        });
    }

    @Test
    public void error() {
        IAbilityDelegator abilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        abilityDelegator.runOnUIThreadSync(new Runnable() {
            @Override
            public void run() {
                Snacky.Builder builder = Snacky.builder();
                PopupDialog dialog = builder.setContext(context)
                        .error();
                dialog.show();
                Snacky.Type type = builder.getType();
                assertEquals(Snacky.Type.ERROR, type);
            }
        });
    }

    @Test
    public void warning() {
        IAbilityDelegator abilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        abilityDelegator.runOnUIThreadSync(new Runnable() {
            @Override
            public void run() {
                Snacky.Builder builder = Snacky.builder();
                PopupDialog dialog = builder.setContext(context)
                        .warning();
                dialog.show();
                Snacky.Type type = builder.getType();
                assertEquals(Snacky.Type.WARNING, type);
            }
        });
    }
}