/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.mateware.snackysample;

import de.mateware.snacky.Snacky;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.colors.RgbPalette;
import ohos.agp.components.Button;
import ohos.agp.text.Font;
import ohos.agp.window.dialog.PopupDialog;
import ohos.app.Context;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ExampleAbilityTest {

    private IAbilityDelegator abilityDelegator;
    private Context context = null;
    ExampleAbility mainAbility;

    @Before
    public void setUp() throws Exception {
        abilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        context = abilityDelegator.getAppContext();
        mainAbility = EventHelper.startAbility(ExampleAbility.class);
        EventHelper.waitForActive(mainAbility,4);
        Thread.sleep(2000);
    }

    @After
    public void tearDown() throws Exception {
        Thread.sleep(2000);
        EventHelper.clearAbilities();
        abilityDelegator.stopAbility(mainAbility);
        context = null;
        abilityDelegator = null;
    }

    @Test
    public void showTest(){
        Button button = (Button) mainAbility.findComponentById(ResourceTable.Id_button_default);
        EventHelper.triggerClickEvent(mainAbility,button);
        Snacky.Builder builder = mainAbility.getBuilder();
        IAbilityDelegator abilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        abilityDelegator.runOnUIThreadSync(new Runnable() {
            @Override
            public void run() {
                PopupDialog dialog = builder.setContext(context)
                        .setText("Snackbar text is not cut after two lines, you can use setMaxLines(int) to achieve this")
                        .setTextColor(RgbPalette.parse("#FFFFFF"))
                        .setActionTextColor(RgbPalette.parse("#FF1493"))
                        .setDuration(Snacky.LENGTH_SHORT)
                        .build();
                dialog.show();
                assertTrue(dialog.isShowing());
            }
        });
    }

    @Test
    public void defaultTest(){
        Button button = (Button) mainAbility.findComponentById(ResourceTable.Id_button_default);
        EventHelper.triggerClickEvent(mainAbility,button);
        Snacky.Builder builder = mainAbility.getBuilder();
        assertEquals(Snacky.Type.DEFAULT, builder.getType());
    }

    @Test
    public void successTest(){
        Button button = (Button) mainAbility.findComponentById(ResourceTable.Id_button_success);
        EventHelper.triggerClickEvent(mainAbility,button);
        Snacky.Builder builder = mainAbility.getBuilder();
        assertEquals(Snacky.Type.SUCCESS, builder.getType());
    }

    @Test
    public void infoTest(){
        Button button = (Button) mainAbility.findComponentById(ResourceTable.Id_button_info);
        EventHelper.triggerClickEvent(mainAbility,button);
        Snacky.Builder builder = mainAbility.getBuilder();
        assertEquals(Snacky.Type.INFO, builder.getType());
    }

    @Test
    public void warningTest(){
        Button button = (Button) mainAbility.findComponentById(ResourceTable.Id_button_warning);
        EventHelper.triggerClickEvent(mainAbility,button);
        Snacky.Builder builder = mainAbility.getBuilder();
        assertEquals(Snacky.Type.WARNING, builder.getType());
    }

    @Test
    public void errorTest(){
        Button button = (Button) mainAbility.findComponentById(ResourceTable.Id_button_error);
        EventHelper.triggerClickEvent(mainAbility,button);
        Snacky.Builder builder = mainAbility.getBuilder();
        assertEquals(Snacky.Type.ERROR, builder.getType());
    }

    @Test
    public void customTest(){
        Button button = (Button) mainAbility.findComponentById(ResourceTable.Id_button_custom);
        EventHelper.triggerClickEvent(mainAbility,button);
        Snacky.Builder builder = mainAbility.getBuilder();
        IAbilityDelegator abilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        abilityDelegator.runOnUIThreadSync(new Runnable() {
            @Override
            public void run() {
                PopupDialog dialog = builder.setContext(context)
                        .setBackgroundColor(RgbPalette.parse("#0077CC"))
                        .setTextSize(42)
                        .setTextColor(RgbPalette.parse("#FFFFFF"))
                        .setTextTypefaceStyle(Font.SERIF)
                        .setText("This is a custom Snackbar with all possibilities of customization and an icon. It will be cutted after 4 lines of text, so it depends on your test device if you can read all of this")
                        .centerText()
                        .setActionText("YEAH!")
                        .build();
                dialog.show();
                assertTrue(dialog.isShowing());
            }
        });
    }
}