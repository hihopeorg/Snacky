package de.mateware.snackysample;

import de.mateware.snacky.Snacky;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbPalette;
import ohos.agp.components.*;
import ohos.agp.text.Font;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

public class ExampleAbility extends Ability {

    private Snacky.Builder builder;
    private DirectionalLayout directionalLayout;
    private EventHandler eventHandler;
    private Runnable delayTask;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        directionalLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_bottom_layout);
        findComponentById(ResourceTable.Id_button_fab).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                builder = Snacky.builder();
                Snacky.SnackyPopup popupDialog = (Snacky.SnackyPopup)(builder.setContext(ExampleAbility.this)
                        .setText("You have to use CoordinatorLayout as usual to have a FAB floating with the Snackbar.")
                        .setDuration(Snacky.LENGTH_SHORT)
                        .setTextColor(RgbPalette.parse("#FFFFFF"))
                        .build());
//                        .show();
                addComponent(popupDialog, Snacky.LENGTH_SHORT, false);

            }
        });


        findComponentById(ResourceTable.Id_button_default).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                builder = Snacky.builder();
                Snacky.SnackyPopup popupDialog = (Snacky.SnackyPopup)builder.setContext(ExampleAbility.this)
                        .setActionText("ACTION")
                        .setActionClickListener(new Component.ClickedListener() {
                            @Override
                            public void onClick(Component defaultView) {
                                System.out.println("----hy---hos=");
                            }
                        })
                        .setText("Snackbar text is not cut after two lines, you can use setMaxLines(int) to achieve this")
                        .setTextColor(RgbPalette.parse("#FFFFFF"))
                        .setActionTextColor(RgbPalette.parse("#FF1493"))
                        .setDuration(Snacky.LENGTH_INDEFINITE)
                        .build();
//                        .show();
                addComponent(popupDialog, Snacky.LENGTH_INDEFINITE, false);

            }
        });

        findComponentById(ResourceTable.Id_button_success).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                builder = Snacky.builder();
                Snacky.SnackyPopup popupDialog = (Snacky.SnackyPopup)builder.setContext(ExampleAbility.this)
                        .setText("Success")
                        .setDuration(Snacky.LENGTH_SHORT)
                        .success();
//                        .show();
                addComponent(popupDialog, Snacky.LENGTH_SHORT, false);

            }
        });

        findComponentById(ResourceTable.Id_button_info).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                builder = Snacky.builder();
                Snacky.SnackyPopup popupDialog = (Snacky.SnackyPopup)builder.setContext(ExampleAbility.this)
                        .setText("Info (with centered text)")
                        .centerText()
                        .setDuration(Snacky.LENGTH_LONG)
                        .info();
//                        .show();
                addComponent(popupDialog, Snacky.LENGTH_LONG, false);

            }
        });

        findComponentById(ResourceTable.Id_button_warning).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                builder = Snacky.builder();
                Snacky.SnackyPopup warningSnackBar = (Snacky.SnackyPopup)builder
                        .setContext(ExampleAbility.this)
                        .setText("Warning with Snackbar.Callback")
                        .setDuration(Snacky.LENGTH_LONG)
                        .setTextSize(AttrHelper.fp2px(14, ExampleAbility.this))
                        .setCallback(new Snacky.Callback() {
                            @Override
                            public void onShown() {
                                new ToastDialog(ExampleAbility.this).setText("onShown Callback").setAlignment(LayoutAlignment.CENTER).show();
                            }

                            @Override
                            public void onDismissed() {
                                new ToastDialog(ExampleAbility.this).setText("onDismissed Callback").setAlignment(LayoutAlignment.CENTER).show();
                            }
                        })
                        .warning();
//                warningSnackBar.show();
                addComponent(warningSnackBar, Snacky.LENGTH_LONG, true);
            }
        });

        findComponentById(ResourceTable.Id_button_error).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                builder = Snacky.builder();
                Snacky.SnackyPopup popupDialog = (Snacky.SnackyPopup)builder.setContext(ExampleAbility.this)
                        .setText("Error")
                        .setDuration(Snacky.LENGTH_INDEFINITE)
                        .setActionText("OK")
                        .error();
//                        .show();
                addComponent(popupDialog, Snacky.LENGTH_INDEFINITE, false);
            }
        });

        findComponentById(ResourceTable.Id_button_custom).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                builder = Snacky.builder();
                Snacky.SnackyPopup popupDialog = (Snacky.SnackyPopup)builder.setContext(ExampleAbility.this)
                        .setBackgroundColor(RgbPalette.parse("#0077CC"))
                        .setTextSize(42)
                        .setTextColor(RgbPalette.parse("#FFFFFF"))
                        .setTextTypefaceStyle(Font.SERIF)
                        .setText("This is a custom Snackbar with all possibilities of customization and an icon. It will be cutted after 4 lines of text, so it depends on your test device if you can read all of this")
                        .centerText()
                        .setActionText("YEAH!")
                        .setActionTextColor(RgbPalette.parse("#66FFFFFF"))
                        .setActionTextSize(48)
                        .setActionTextTypefaceStyle(Font.DEFAULT_BOLD)
                        .setIcon(ResourceTable.Media_icon)
                        .setDuration(Snacky.LENGTH_INDEFINITE)
                        .setMaxLines(4)
                        .build();
//                        .show();
                addComponent(popupDialog, Snacky.LENGTH_INDEFINITE, false);
            }
        });
        eventHandler = new EventHandler(EventRunner.getMainEventRunner());
        delayTask = new Runnable() {
            @Override
            public void run() {
                if (directionalLayout.getChildCount() > 1) {
                    directionalLayout.removeComponentAt(1);
                }
            }
        };
    }

    public Snacky.Builder getBuilder() {
        return builder;
    }

    // 此方法仅为适配底部圆形控件调用，library正常使用不需要调用该方法
    private void addComponent(Snacky.SnackyPopup popupDialog, long time, boolean isNeedCallback) {
        Component customComponent = popupDialog.getCustomComponent();
        customComponent.setMarginBottom(AttrHelper.vp2px(8, ExampleAbility.this));
        if (eventHandler.hasInnerEvent(delayTask)) {
            eventHandler.removeTask(delayTask);
        }
        if (isNeedCallback) {
            directionalLayout.setLayoutRefreshedListener(new Component.LayoutRefreshedListener() {
                @Override
                public void onRefreshed(Component component) {
                    int childCount = directionalLayout.getChildCount();
                    if (childCount > 1) {
                        new ToastDialog(ExampleAbility.this).setText("onShown Callback").setAlignment(LayoutAlignment.CENTER).show();
                    } else {
                        new ToastDialog(ExampleAbility.this).setText("onDismissed Callback").setAlignment(LayoutAlignment.CENTER).show();
                    }
                }
            });
        } else {
            directionalLayout.setLayoutRefreshedListener(null);
        }
        if (directionalLayout.getChildCount() > 1) {
            directionalLayout.removeComponentAt(1);
            directionalLayout.addComponent(customComponent, 1);
        } else {
            directionalLayout.addComponent(customComponent, 1);
        }
        if (time > 0) {
            eventHandler.postTask(delayTask, time);
        } else {
            Button actionText = (Button) customComponent.findComponentById(de.mateware.snacky.ResourceTable.Id_action);
            if (actionText != null) {
                actionText.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        directionalLayout.removeComponentAt(1);
                    }
                });
            }
        }
    }
}

