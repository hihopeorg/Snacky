package de.mateware.snacky;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.element.Element;
import ohos.agp.render.BlendMode;
import ohos.agp.render.ColorMatrix;

public class SnackyUtils {
    static Element tintDrawable(Element drawable, int color) {
        RgbColor rgbColor = RgbColor.fromArgbInt(color);
        ColorMatrix colorMatrix = new ColorMatrix();
        float[] srcColor = new float[]{
                1,0,0,0,rgbColor.getRed(),
                0,1,0,0,rgbColor.getGreen(),
                0,0,1,0,rgbColor.getBlue(),
                0,0,0,1,rgbColor.getAlpha()
        };
        colorMatrix.setMatrix(srcColor);
        drawable.setColorMatrix(colorMatrix);
        drawable.setStateColorMode(BlendMode.COLOR_BURN);

        return drawable;
    }

    /*static Element makeTransparentDrawable(Context context, int width, int heigth) {
        PixelMap conf = PixelMap.alphaType.ARGB_8888; // see other conf types
        //PixelMap bmp = PixelMap.createBitmap(width, heigth, conf);
        PixelMap bmp = PixelMap.create(conf,width, heigth);
        return new BitmapDrawable(context.getResourceManager(),bmp);
    }*/
}
