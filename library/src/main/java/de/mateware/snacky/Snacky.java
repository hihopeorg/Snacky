package de.mateware.snacky;

import de.mateware.snacky.ResourceTable;
import ohos.agp.colors.RgbColor;
import ohos.agp.colors.RgbPalette;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.VectorElement;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.TextAlignment;
import ohos.agp.window.dialog.PopupDialog;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class Snacky {

    private int defTextSize = 44;
    private int defActionTextSize = 48;
    //private int centerWidth = 400;
    private int leftWidth = 200;
    private int topWidth = 5;

    public enum Type {
        DEFAULT(null, null, null),
        SUCCESS(RgbPalette.parse("#388E3C"), ResourceTable.Graphic_ic_check_black_24dp, RgbPalette.parse("#FFFFFF")),
        ERROR(RgbPalette.parse("#D50000"), ResourceTable.Graphic_ic_clear_black_24dp, RgbPalette.parse("#FFFFFF")),
        INFO(RgbPalette.parse("#3F51B5"), ResourceTable.Graphic_ic_info_outline_black_24dp, RgbPalette.parse("#FFFFFF")),
        WARNING(RgbPalette.parse("#FFA900"), ResourceTable.Graphic_ic_error_outline_black_24dp, RgbPalette.parse("#000000"));


        private Integer color;
        private Integer iconResId;
        private Integer standardTextColor;

        Type(Integer color, Integer iconResId, Integer standardTextColor) {
            this.color = color;
            this.iconResId = iconResId;
            this.standardTextColor = standardTextColor;
        }

        public Integer getColor() {
            return color;
        }

        public Element getIcon(Context context) {
            if (iconResId == null) return null;
            VectorElement drawable = new VectorElement(context, iconResId);
            if (drawable != null) {
                //整个icon变色
//                drawable = SnackyUtils.tintDrawable(drawable, standardTextColor);
            }
            return drawable;
        }

        public Integer getStandardTextColor() {
            return standardTextColor;
        }
    }

    public final Builder builder;
    public final Context context;

    public Snacky(Builder builder, Context context) {
        this.builder = builder;
        this.context = context;
    }

    public SnackyPopup make() {
        Component component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_default_snacky, null, false);
        SnackyPopup snackbar = new SnackyPopup(context, component);
        int screenWidth = DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes().width;
        int leftSpace = AttrHelper.vp2px(8, context);
        int width = screenWidth - leftSpace - leftSpace;
        snackbar.setCustomComponent(component)
                .setText(builder.text)
                .setAlignment(LayoutAlignment.BOTTOM | LayoutAlignment.HORIZONTAL_CENTER)
                .setSize(width, ComponentContainer.LayoutConfig.MATCH_CONTENT);
        if (builder.textColor == null) builder.textColor = builder.type.getStandardTextColor();
        if (builder.actionTextColor == null) builder.actionTextColor = builder.type.getStandardTextColor();
        component.setMarginBottom(AttrHelper.vp2px(44, context));
        snackbar.setCustomComponent(component);
        if (builder.duration == Snacky.LENGTH_INDEFINITE) {
        } else {
            snackbar.setDuration(builder.duration);
        }

        Component snackbarLayout = component;

        if (builder.backgroundColor == null) builder.backgroundColor = builder.type.getColor();
        if (builder.backgroundColor != null) {
            ShapeElement shapeElement = new ShapeElement();
            shapeElement.setRgbColor(RgbColor.fromArgbInt(builder.backgroundColor));
            snackbarLayout.setBackground(shapeElement);
        }

        snackbar.setCallback(new Callback() {
            @Override
            public void onShown() {
                if (builder.callback != null) {
                    builder.callback.onShown();
                }
            }

            @Override
            public void onDismissed() {
                if (builder.callback != null) {
                    builder.callback.onDismissed();
                }
            }
        });
        if ((snackbarLayout.findComponentById(ResourceTable.Id_action)) != null) {
            Button actionText = (Button) snackbarLayout.findComponentById(ResourceTable.Id_action);
            if (builder.actionClickListener != null || builder.actionText != null) {
                actionText.setVisibility(Component.VISIBLE);
                if (builder.actionClickListener == null) {
                    builder.actionClickListener = new Component.ClickedListener() {
                        @Override
                        public void onClick(Component v) {
                        }
                    };
                }
                actionText.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        snackbar.destroy();
                        builder.actionClickListener.onClick(component);               }
                });
            }
            if (builder.actionText != null) {
                actionText.setText(builder.actionText);
            } else {
                actionText.setText(" ");
            }

            if (builder.actionTextColor == null) {
                builder.actionTextColor = builder.type.getStandardTextColor();
            } else {
                actionText.setTextColor(new Color(builder.actionTextColor));
            }

            if (builder.actionTextSize != null) {
                actionText.setTextSize(builder.actionTextSize);
            } else {
                actionText.setTextSize(defActionTextSize);
            }

            Font actionTextTypeface = actionText.getFont();

            if (builder.actionTextTypefaceStyle != null) {
                actionText.setFont(builder.actionTextTypefaceStyle);
            } else {
                actionText.setFont(actionTextTypeface);
            }
        }


        if ((snackbarLayout.findComponentById(ResourceTable.Id_text)) != null) {
            Text text = (Text) snackbarLayout.findComponentById(ResourceTable.Id_text);
            text.setMultipleLine(true);
            if (builder.text != null) {
                text.setText(builder.text);
            } else {
                text.setText(" ");
            }
            // text.setHeight();

            if (builder.textSize != null) {
                text.setTextSize(builder.textSize);
            } else {
                text.setTextSize(defTextSize);
            }

            Font textTypeface = text.getFont();

            if (builder.textTypefaceStyle != null) {
                text.setFont(builder.textTypefaceStyle);
            } else {
                text.setFont(textTypeface);
            }

            if (builder.textColor == null) {
                builder.textColor = builder.type.getStandardTextColor();
            } else {
                text.setTextColor(new Color(builder.textColor));
            }

            if (builder.maxLines != 0) {
                text.setMaxTextLines(builder.maxLines);
                text.setTruncationMode(Text.TruncationMode.ELLIPSIS_AT_END);
            }

            // text.setGravity(builder.centerText ? TableLayout.Alignment.CENTER : Gravity.CENTER_VERTICAL);

            if (builder.centerText) {
                // text.setWidth(centerWidth);
                //text.setMarginTop(topWidth);
                if (builder.icon == null) {
                    text.setMarginLeft(leftWidth);
                } else {
                    text.setMarginLeft(builder.icon.getWidth());
                }

                text.setTextAlignment(builder.centerText ? TextAlignment.CENTER : TextAlignment.VERTICAL_CENTER);
            }
            if (builder.icon == null) builder.icon = builder.type.getIcon(builder.context);
            if (builder.icon != null) {
                Element transparentHelperDrawable = null;
                if (builder.centerText && TextUtils.isEmpty(builder.actionText)) {

                    /*transparentHelperDrawable = SnackyUtils.makeTransparentDrawable(builder.view.getContext(),
                            builder.icon.getWidth(),
                            builder.icon.getHeight());*/
                    text.setMarginLeft(10);
                    text.setTextAlignment(TextAlignment.CENTER);
                    text.setAroundElements(builder.icon, null, null, null);
                    text.setAroundElementsPadding(AttrHelper.vp2px(16, context));
                } else {
                    text.setAroundElements(builder.icon, null, transparentHelperDrawable, null);
                    text.setAroundElementsPadding(AttrHelper.vp2px(16, context));
                }

            }
        }

        return snackbar;
    }

    public static Builder builder() {
        return new Builder();
    }


    public interface Callback {
        void onShown();

        void onDismissed();
    }

    @Retention(RetentionPolicy.SOURCE)
    public @interface Duration {
    }

    public static final int LENGTH_INDEFINITE = -1;
    public static final int LENGTH_SHORT = 2000;
    public static final int LENGTH_LONG = 3000;


    public static class Builder {

        private Context context = null;
        private Type type = Type.DEFAULT;
        private int duration = LENGTH_SHORT;
        private String text = "";
        private int textResId = 0;
        private Integer textColor = null;
        private Integer textSize = null;
        private Font textTypefaceStyle = null;
        private Font textTypeface = null;
        private Integer actionTextSize = null;
        private String actionText = "";
        private int actionTextResId = 0;
        private Font actionTextTypefaceStyle = null;
        private Font actionTextTypeface = null;
        private Component.ClickedListener actionClickListener = null;
        private Integer actionTextColor = null;
        private int maxLines = Integer.MAX_VALUE;
        private boolean centerText = false;
        private Element icon = null;
        private int iconResId = 0;
        private Integer backgroundColor = null;
        public Callback callback;

        private Builder() {
        }

        public Type getType() {
            return type;
        }

        public Builder setCallback(Callback callback) {
            this.callback = callback;
            return this;
        }

        public Builder setContext(Context context) {
            this.context = context;
            return this;
        }

        public Builder setText(int resId) {
            this.textResId = resId;
            return this;
        }

        public Builder setText(String text) {
            this.textResId = 0;
            this.text = text;
            return this;
        }

        public Builder setTextColor(int color) {
            this.textColor = color;
            return this;
        }

        public Builder setTextSize(int textSize) {
            this.textSize = textSize;
            return this;
        }

        public Builder setTextSize(int unit, int textSize) {
            this.textSize = textSize;
            return this;
        }

        public Builder setTextTypeface(Font typeface) {
            this.textTypeface = typeface;
            return this;
        }

        public Builder setTextTypefaceStyle(Font style) {
            this.textTypefaceStyle = style;
            return this;
        }

        public Builder centerText() {
            this.centerText = true;
            return this;
        }

        public Builder setActionTextColor(int color) {
            this.actionTextColor = color;
            return this;
        }

        public Builder setActionText(int resId) {
            this.actionTextResId = resId;
            return this;
        }

        public Builder setActionText(String text) {
            this.textResId = 0;
            this.actionText = text;
            return this;
        }

        public Builder setActionTextSize(int textSize) {
            this.actionTextSize = textSize;
            return this;
        }

        public Builder setActionTextSize(int unit, int textSize) {
            this.actionTextSize = textSize;
            return this;
        }

        public Builder setActionTextTypeface(Font typeface) {
            this.actionTextTypeface = typeface;
            return this;
        }


        public Builder setActionTextTypefaceStyle(Font style) {
            this.actionTextTypefaceStyle = style;
            return this;
        }

        public Builder setActionClickListener(Component.ClickedListener listener) {
            this.actionClickListener = listener;
            return this;
        }

        public Builder setMaxLines(int maxLines) {
            this.maxLines = maxLines;
            return this;
        }

        public Builder setDuration(@Duration int duration) {
            this.duration = duration;
            return this;
        }

        public Builder setIcon(int resId) {
            this.iconResId = resId;
            return this;
        }

        public Builder setIcon(Element drawable) {
            this.icon = drawable;
            return this;
        }

        public Builder setBackgroundColor(int color) {
            this.backgroundColor = color;
            return this;
        }

        public PopupDialog build() {
            return make();
        }

        public PopupDialog success() {
            type = Type.SUCCESS;
            return make();
        }

        public PopupDialog info() {
            type = Type.INFO;
            return make();
        }

        public PopupDialog warning() {
            type = Type.WARNING;
            return make();
        }

        public PopupDialog error() {
            type = Type.ERROR;
            return make();
        }

        private PopupDialog make() {
            if (context == null)
                throw new IllegalStateException("Snacky Error: You must set a context before making a snack");
            if (textResId != 0) {
                try {
                    text = context.getResourceManager().getElement(textResId).getString();
                } catch (IOException | NotExistException | WrongTypeException e) {
                    e.printStackTrace();
                }
            }
            if (actionTextResId != 0) {
                try {
                    actionText = context.getResourceManager().getElement(textResId).getString();
                } catch (IOException | NotExistException | WrongTypeException e) {
                    e.printStackTrace();
                }
            }
            if (iconResId != 0) {
                try {
                    icon = new PixelMapElement(context.getResourceManager().getResource(iconResId));
                } catch (IOException | NotExistException e) {
                    e.printStackTrace();
                }
            }

            return new Snacky(this, context).make();
        }
    }

    public static class SnackyPopup extends PopupDialog {

        private Callback callback;
        private Component customComponent;

        public SnackyPopup(Context context, Component contentComponent) {
            super(context, contentComponent);
        }

        public SnackyPopup(Context context, Component contentComponent, int width, int height) {
            super(context, contentComponent, width, height);
        }

        @Override
        public PopupDialog setCustomComponent(Component customComponent) {
            this.customComponent = customComponent;
            return super.setCustomComponent(customComponent);
        }

        public Component getCustomComponent() {
            return customComponent;
        }

        @Override
        public void show() {
            super.show();
            if (callback != null) {
                callback.onShown();
            }
        }

        @Override
        public void destroy() {
            super.destroy();
            if (callback != null) {
                callback.onDismissed();
            }
        }

        public void setCallback(Callback callback) {
            this.callback = callback;
        }
    }
}
